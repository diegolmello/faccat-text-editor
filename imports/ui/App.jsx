import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import ReactQuill from 'react-quill';

// App component - represents the whole app
class App extends Component {
  constructor(props) {
    super(props);
    this.state = { text: '' };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(value) {
    console.log(value);
    this.setState({ text: value });
  }

  render() {
    return (
      <div className="container">
        <ReactQuill theme="snow" value={this.state.text} onChange={this.handleChange} />
      </div>
    );
  }
}

export default createContainer(() => {
  return {};
}, App);
